package com.groundgurus.day2;

public class Shop {
    // attributes have initial values
    // byte = (byte) 0
    // short = (short) 0
    // int = 0
    // long = 0L
    // float = 0.0F
    // double = 0.0
    // boolean = false
    // reference type = null

    String name;
    boolean areMinorsAllowed;
    String owner;

    public Shop(String name) {
        this.name = StringUtil.trim(name);
    }

    @Override
    public String toString() {
        return "Name: " + name;
    }
}
