package com.groundgurus.day2;

public class StringUtil {
    public static String trim(String str) {
        if (str == null) {
            return "";
        }

        return str.trim();
    }
}
