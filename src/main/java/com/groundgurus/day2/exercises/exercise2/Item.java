package com.groundgurus.day2.exercises.exercise2;

public class Item {
    String name;
    double price;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
