package com.groundgurus.day2.exercises.exercise2;

public class Day2Exercise2 {
    public static void main(String[] args) {
        var smSupermarket = new Supermarket();
        smSupermarket.fillTheInventory();

        var joe = new Customer("Joe", 500.00);
        var eggs = smSupermarket.findItem("eggs");
        joe.addToCart(eggs);

        var butter = smSupermarket.findItem("butter");
        joe.addToCart(butter);

        var bread = smSupermarket.findItem("bread");
        joe.addToCart(bread);

        joe.checkout(smSupermarket);
    }
}
