package com.groundgurus.day2.exercises.exercise2;

public class Customer {
    String name;
    double cash;
    Item[] cart = new Item[10];
    int count;

    public Customer(String name, double cash) {
        this.name = name;
        this.cash = cash;
    }

    public void addToCart(Item item) {
        cart[count] = item;
        count++;
    }

    public void checkout(Supermarket supermarket) {
        supermarket.checkout(this, cart);
    }
}
