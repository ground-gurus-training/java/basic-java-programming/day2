package com.groundgurus.day2.exercises.exercise2;

public class Supermarket {
    Item[] items = new Item[10];

    public Item findItem(String name) {
        for (Item item : items) {
            if (item != null && item.name.contains(name)) {
                return item;
            }
        }

        // default return if the item name is not found
        return null;
    }

    public void fillTheInventory() {
        items[0] = new Item("1 dozen of eggs", 90.00);
        items[1] = new Item("butter (small)", 20.00);
        items[2] = new Item("pack of bread", 40.00);
    }

    public void checkout(Customer customer, Item[] cart) {
        System.out.println("Customer: " + customer.name);
        System.out.println("Cash: " + customer.cash);

        var amount = 0.0;
        for (var item : cart) {
            if (item != null) {
                System.out.println(item.name + ", " + item.price);
                amount += item.price;
            }
        }

        var change = customer.cash - amount;
        customer.cash = change;
        System.out.println("Change: " + change);
    }
}
