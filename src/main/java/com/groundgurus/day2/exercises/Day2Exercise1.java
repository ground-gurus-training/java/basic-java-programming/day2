package com.groundgurus.day2.exercises;

import java.util.Scanner;

public class Day2Exercise1 {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Enter a value: ");
        var value = scanner.nextInt();

        int[] myNumbers = {5, 6, 4, 12, 10};
        var index = -1;

        index = findIndex(value, myNumbers, index);

        if (index != -1) {
            System.out.println(value + " is in the index " + index);
        } else {
            System.out.println(value + " cannot be found");
        }
    }

    private static int findIndex(int value, int[] myNumbers, int index) {
        for (int i = 0; i < myNumbers.length; i++) {
            if (myNumbers[i] == value) {
                return i;
            }
        }
        return -1;
    }
}
