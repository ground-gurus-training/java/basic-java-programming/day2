package com.groundgurus.day2;

public class ArrayExample {
    public static void main(String[] args) {
        // declaration of an array and an initial value
        int stuff[] = { 11, 22, 33, 44, 55 };

        // declaration of an array
        int stuff2[];
        // set an initial value
        stuff2 = new int[] { 11, 22, 33, 44, 55 };

        // declare array and assigned an int array with the size of 50
        int stuff3[] = new int[50]; // 50 elements, indexes from 0-49

        // if you create an array with a size, their initial values differs based on their type
        // byte = (byte) 0
        // short = (short) 0
        // int = 0
        // long = 0L
        // float = 0.0F
        // double = 0.0
        // boolean = false
        // reference type = null

        System.out.println(stuff3[34]);
    }
}
