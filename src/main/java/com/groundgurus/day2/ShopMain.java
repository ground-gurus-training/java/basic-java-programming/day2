package com.groundgurus.day2;

public class ShopMain {
    public static void main(String[] args) {
        // initialize the objects and set a value to name
        Shop cakeShop = new Shop("Goldilocks");
        Shop flowerShop = new Shop("   Claire's Flowers   ");
        Shop wineShop = new Shop("   Joe's Winery   ");

        // print out the values
        System.out.println(cakeShop);
        System.out.println(flowerShop);
        System.out.println(wineShop);
    }
}
